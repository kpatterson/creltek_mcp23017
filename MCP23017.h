#ifndef _Creltek_MCP23017_h_
#define _Creltek_MCP23017_h_
// ---------------------------------------------------------------------------
// Copyright (C) 2015 Kevin H. Patterson
// Created on 2015-12-27.
//
// @license
// Creltek::MCP23017 by Kevin H. Patterson is licensed under a Creative
// Commons Attribution-ShareAlike 4.0 International License.
// Based on a work at https://bitbucket.org/kpatterson/creltek_mcp23017
//
// This software is furnished "as is", without technical support, and with no
// warranty, express or implied, as to its usefulness for any purpose.
//
// Thread Safe: No
// Extendable: Yes
//
// @file MCP23017.h
// Provides a driver for the Microchip MCP23017 i2c GPIO extender IC.
//
// @dependencies
// Standard Arduino libraries
//
// @author Kevin H. Patterson - kevpatt _at_ khptech _dot_ com
// ---------------------------------------------------------------------------

#include <Wire.h>
#include <inttypes.h>


#define MCP23017_READ 0x1
#define MCP23017_WRITE 0x0

#define MCP23017_IODIRA 0x00
#define MCP23017_IODIRB 0x01
#define MCP23017_IPOLA 0x02
#define MCP23017_IPOLB 0x03
#define MCP23017_GPINTENA 0x04
#define MCP23017_GPINTENB 0x05
#define MCP23017_GPIOA 0x12
#define MCP23017_GPIOB 0x13
#define MCP23017_OLATA 0x14
#define MCP23017_OLATB 0x15


namespace Creltek {

	class MCP23017 {
	public:
		MCP23017( uint8_t i_Address = 0b0000111 )
		: m_Address( 0b0100000 | (i_Address & 0b0000111) )
		{}

		uint8_t address() const {
			return m_Address;
		}

		void init() {
			// set data direction
			{
				Wire.beginTransmission( m_Address );
				Wire.write( MCP23017_IODIRA );
				Wire.write( 0x00 );
				Wire.endTransmission();

				Wire.beginTransmission( m_Address );
				Wire.write( MCP23017_IODIRB );
				Wire.write( 0x00 );
				Wire.endTransmission();
			}

			// set initial state
			{
				write( 0, 0x00 );
				write( 1, 0x00 );
			}
		}

		uint8_t read( uint8_t i_Port ) const {
			if( i_Port > 1 ) return 0;
			return 0;
		}

		uint8_t write( uint8_t i_Port, uint8_t i_Data ) {
			if( i_Port > 1 ) return 0;
			Wire.beginTransmission( m_Address );
			Wire.write( MCP23017_GPIOA + i_Port );
			Wire.write( i_Data );
			Wire.endTransmission();
			m_Data[i_Port] = i_Data;
			return i_Data;
		}

		uint8_t quickRead( uint8_t i_Port ) const {
			if( i_Port > 1 ) return 0;
			return m_Data[i_Port];
		}
		
	private:
		const uint8_t m_Address;
		uint8_t m_Data[2];
	};

}

#endif // _MCP23017_h_
